#include <fstream>
#include <iostream>

using namespace std;

int main() {
    char currentSymbol;
    fstream fin ("./fs/input/test.in", ios::in);
    fstream fout ("./fs/output/test.out", ios::out);

    do {
        fin.get(currentSymbol);
        cout << currentSymbol;
        fout << currentSymbol;
    } while (fin);

    fin.close();

}