#include <fstream>
#include <iostream>

using namespace std;


struct Officer {
    unsigned int key;
    unsigned int inspectionTime;
    int id;
    bool initiated = false;
    Officer(){
        key = -13;
        inspectionTime = -13;
    }
};

struct Passenger {
    public:
        unsigned int arrivalTime;
        unsigned int customsClearedTime;
        int visistedOfficerId;
};

// A utility function to swap two elements 
void swap(Officer **x, Officer **y) { 
    Officer* temp = *x; 
    *x = *y; 
    *y = temp; 
} 

// // A utility function to swap two elements
// void swap(int *x, int *y)
// {
//     int temp = *x;
//     *x = *y;
//     *y = temp;
// }

// A class for Min Heap 
class MinHeap { 
    Officer *heap_array; // pointer to Officer elements in heap 
    int capacity; // maximum possible size of min heap 
    int heap_size; // Current number of elements in min heap 

    public:
        MinHeap(int cap) { 
            heap_size = 0; 
            capacity = cap; 
            heap_array = new Officer[cap]; 
        }  

        // to heapify a subtree with the root at given index 
        void MinHeapify(int i) { 
            int l = left(i); 
            int r = right(i); 
            int smallest = i; 
            if (l < heap_size 
                && (heap_array[l].key < heap_array[i].key
                || (heap_array[l].key == heap_array[i].key && heap_array[l].id < heap_array[i].id))
            ) 
                smallest = l; 
            if (r < heap_size 
                && (heap_array[r].key < heap_array[smallest].key
                || (heap_array[r].key == heap_array[smallest].key && heap_array[r].id < heap_array[smallest].id))
            ) 
                smallest = r; 
            if (smallest != i) { 
                swap(heap_array[i], heap_array[smallest]); 
                MinHeapify(smallest); 
            } 
        } 

        int parent(int i) { return (i-1)/2; } // floor() auto 

        // to get index of left child of node at index i 
        int left(int i) { return (2*i + 1); } 

        // to get index of right child of node at index i 
        int right(int i) { return (2*i + 2); } 

        // to extract the root which is the minimum element 
        Officer extractMin() { 
            if (heap_size <= 0) 
                return Officer(); 
            if (heap_size == 1) { 
                heap_size--; 
                return heap_array[0]; 
            } 
        
            // Store the minimum value, and remove it from heap 
            Officer root = heap_array[0]; 
            heap_array[0] = heap_array[heap_size-1]; 
            heap_size--; 
            MinHeapify(0); 
        
            return root; 
        } 

        // // Decreases key value of key at index i to new_val 
        // void decreaseKey(int i, int new_val); 

        // Returns the minimum key (key at root) from min heap 
        Officer getMin() { return heap_array[0]; } 

        // // Deletes a key stored at index i 
        // void deleteKey(int i); 

        // Inserts a new key 'k' 
        void insertKey(Officer k) { 
            if (heap_size == capacity) { 
                cout << "\nOverflow: Could not insertKey\n"; 
                return; 
            } 

            // First insert the new key at the end 
            heap_size++; 
            int i = heap_size - 1; 
            heap_array[i] = k; 
        
            // Fix the min heap property if it is violated 
            while (i != 0 
                    && (heap_array[parent(i)].key > heap_array[i].key
                        || (heap_array[parent(i)].key == heap_array[i].key && heap_array[parent(i)].id > heap_array[i].id)
                        )
                    ) { 
                swap(heap_array[i], heap_array[parent(i)]); 
                i = parent(i); 
            } 
        }
};



class Node {
  public:
        Passenger value; //Elementa vērtības lauks
        Node *next; //Nākošā saraksta elementa norāde
        Node *previous ;
        Node(Passenger val){
            value = val;
            next = NULL;
            previous = NULL;
        }
        ~Node(){
            cout << "Deleted" << endl;
        }
};

// Two way linked list taken from 
class TWLList {
    protected:
        Node *first;
        Node *last;
    public:
        Node *current;
        Node *tempNodeStorage;

    TWLList(){ //Kontrukstors ar value lauka aizpildi
        first = NULL;
        last = NULL;
        current = NULL;
    }

    void addNext(Passenger val) {
        Node *node = new Node(val);
        if (first == NULL) {
            first = last = node;
        } else {
            last->next = node;
            node->previous = last;
            last = node;
        }
    }

    bool isEmpty() {
        return (first == NULL);
    }

    void start() {
        current = first;
    }

    bool end() {
        return (current == NULL);
    }

    void next() {
        if (!end()) {
            current = current->next;
        }
    }

    void setIteratorToEnd() {
        current = last;
    }

    void iteratorPrevious() {
        current = current->previous;
    }

    void clear_list() {
        Node *t = first;
        if (!isEmpty()) {
            if (current == first) {
                current = first->next;
            }
            first = first->next;
            delete t;
            if (isEmpty()) {
                last = NULL;
            }
        }
    }

    void sortList() {
        setIteratorToEnd();
        while (true){
            if (current == first) return; // No elements left

            if (current->value.customsClearedTime > current->previous->value.customsClearedTime) return; // List sorted
            if (current->value.customsClearedTime == current->previous->value.customsClearedTime
                && current->value.visistedOfficerId > current->previous->value.visistedOfficerId
                ) return;
            if (current->previous == first){
                first->previous = current;
                if (current->next == NULL ){
                    first->next = NULL;
                } else {
                    first->next->next->previous = first;
                    first->next = first->next->next;
                }
                current->next = first;
                current->previous = NULL;
                if (current == last) {
                    cout << "FIRST\n";
                    last = current->next;
                }
                
                first = current;
                break;
            }
            tempNodeStorage = current->previous; // set temp

            tempNodeStorage->previous->next = current; // attach one before
            current->previous = tempNodeStorage->previous; // link current to previous previous
            if (current != last) {
                current->next->previous = tempNodeStorage; //attach one after to swapped
                tempNodeStorage->next = current->next;
            }
            if (current == last) {
                last = tempNodeStorage; // make swapped last
                tempNodeStorage->next = NULL;
            } // detach next of swapped
            current->next = tempNodeStorage; // set current before 
            tempNodeStorage->previous = current; // attach swaped
        }
    }
};

// Helper for debuging
void printPassengersList (TWLList list) {
    Passenger out;
    cout << "Print\n";
    for (list.start(); !list.end(); list.next()) {
        out = list.current->value;
        cout << out.arrivalTime << " " << out.customsClearedTime << endl;
    }
    cout << endl;
}

// Helper for debuging
void printOfficersHeap (MinHeap officersHeap, int officersCount, bool debug = false) {
    Officer out; // used for printing values

    if (debug) {
        officersCount += 10;
    }

    for (int i = 0; i < officersCount; i++) {
        out = officersHeap.extractMin();
        cout << i+1 << ". key: "<< out.key << " inspection:" << out.inspectionTime <<" \n";
    }
}

// Taken from post.cpp
bool isLetter(char inputChar){
    if ((inputChar >= 65 && inputChar <= 90) ||
        (inputChar >= 97 && inputChar <= 122)) {
        return true;
    }
    return false;
}

bool isNumber(char inputChar){
    if ((inputChar >= 48 && inputChar <= 57)) {
        return true;
    }
    return false;
}

int checkFirstLetter(char inputSymbol) {
    if (inputSymbol==0){
        return -13;
    }
    if (!isNumber(inputSymbol)){
        return -13;
    }
    return 0;
}

Officer* createOfficersTable(unsigned int officerCount, unsigned int officerDefaultInspectionTime, char tableType){
    Officer* table = new Officer[officerCount];
    int sorter = tableType == 'P' ? 0 : 200;
    for (int i = 0; i < officerCount; i++) {
        table[i].inspectionTime = officerDefaultInspectionTime;
        table[i].id = i+1 + sorter;
        table[i].key = 0; // starts with 1
    }
    return table;
}

void officersTableToHeap(Officer* table, MinHeap heap, int officerCount){
    for (int i = 0; i < officerCount; i++) {
        heap.insertKey(table[i]);
    }
}

class Buffer {
    char* bufferArray;
    int bufferPosition = 0;
    public: 
        int bufferLength = 32;
        Buffer(int length) {
            bufferLength = length;
            bufferArray = new char[length];
        }
        char* get() {
            return bufferArray;
        }

        unsigned int getAsUnsignedInt() {
            return strtoul (bufferArray, NULL, 0);
        }

        void read(char currentChar){
            bufferArray[bufferPosition] = currentChar;
            bufferPosition++;
        }

        void clear(){
            for (int i = 0; i < bufferLength; i++) {
                if (bufferArray[i] == '\0')
                    break;
                bufferArray[i] = '\0';
            }
            bufferPosition = 0;
        }
};

/*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*/
/*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*/
/*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*MAIN*/
int main(){
    // File
    char currentSymbol;
        // fstream fin ("./fs/input/customs.in", ios::in);
        // fstream fout ("./fs/output/customs.out", ios::out);

        fstream fin ("customs.in", ios::in);
        fstream fout ("customs.out", ios::out);
    bool firstLineRead = false;
    bool officerHeapInitiated = false;
    bool onePassengerRead = false;
    char lineStartsWith;
    int linePosition = 1;
    Buffer readBuffer(32);

    // Officers
    char officerType;
    unsigned int officerId;
    unsigned int officerInspectionTime;
    //Heap is created after initial values are read
    MinHeap* officerHeaps[2]; // 0 = N, 1 = P
    Officer* officerTables[2]; // 0 = N, 1 = P
    unsigned int officerCounts[2]; // 0 = N, 1 = P
    unsigned int officerDefaultInspectionTimes[2]; // 0 = N, 1 = P
    int officerInitiatedCounts[2] = {0,0}; //Amount of officers initiated 0 = N, 1 = P
    int officerCatSwitch;  // 0 = N, 1 = P


    Officer currentOfficer;

    // Passangers
    // char currentPassangerType;
    // unsigned int currentPassangerArrivalTime;

    // Passengers sorted lit to print
    TWLList passengersListSorted;

    fin.get(currentSymbol);

    if (checkFirstLetter(currentSymbol) == -13){
        fout << "nothing";
        return 0;
    }
    readBuffer.read(currentSymbol);

    while (fin.get(currentSymbol) && currentSymbol != 'X') {
        if (firstLineRead
            && linePosition == 0
        ) {
            lineStartsWith = currentSymbol;
            linePosition++;
        }
        if (currentSymbol == ' ' || currentSymbol == '\n'){
            if (! firstLineRead){ // SET DEFAULT VALUES
                switch (linePosition) 
                {
                case 1:
                    officerCounts[1] = readBuffer.getAsUnsignedInt();
                    break;
                case 2:
                    officerCounts[0] = readBuffer.getAsUnsignedInt();
                    break;
                case 3:
                    officerTables[1] = createOfficersTable(officerCounts[1], readBuffer.getAsUnsignedInt(), 'P');
                    break;
                case 4:
                    officerTables[0] = createOfficersTable(officerCounts[0], readBuffer.getAsUnsignedInt(), 'N');
                    firstLineRead = true;
                    linePosition = -1; //quick fix to get it to 1 :D
                    break;
                default:
                    break;
                }
            }
            if (lineStartsWith == 'T' && linePosition > 1){
                switch (linePosition)
                {
                case 2:
                    officerType = readBuffer.get()[0];
                    break;
                case 3:
                    officerId = readBuffer.getAsUnsignedInt();
                    break;
                case 4:
                    officerInspectionTime = readBuffer.getAsUnsignedInt();
                    officerType == 'P' 
                        ? officerTables[1][officerId - 1].inspectionTime = officerInspectionTime 
                        : officerTables[0][officerId - 1].inspectionTime = officerInspectionTime;
                    linePosition = -1;
                    break;
                default:
                    break;
                }
            }
            if ((lineStartsWith == 'N' || lineStartsWith == 'P') && linePosition > 1){
                if (! officerHeapInitiated){
                    officerHeaps[0] = new MinHeap(officerCounts[0]);
                    officerHeaps[1] = new MinHeap(officerCounts[1]);
                    officerHeapInitiated = true;
                    onePassengerRead = true;
                }
                Passenger* currentPassenger = new Passenger();
                currentPassenger->arrivalTime = readBuffer.getAsUnsignedInt();

                officerCatSwitch = lineStartsWith == 'N' ? 0 : 1;
                currentOfficer = officerHeaps[officerCatSwitch]->extractMin();
                if (currentOfficer.inspectionTime == 4294967283) {
                    currentOfficer = officerTables[officerCatSwitch][officerInitiatedCounts[officerCatSwitch]];
                    officerInitiatedCounts[officerCatSwitch]++;
                    currentOfficer.initiated = true;
                } else if (currentOfficer.initiated 
                            && officerInitiatedCounts[officerCatSwitch] != officerCounts[officerCatSwitch]
                            && currentOfficer.key > currentPassenger->arrivalTime
                            ){
                    officerHeaps[officerCatSwitch]->insertKey(currentOfficer);
                    currentOfficer = officerTables[officerCatSwitch][officerInitiatedCounts[officerCatSwitch]];
                    officerInitiatedCounts[officerCatSwitch]++;
                    currentOfficer.initiated = true;
                }
                unsigned int inspectionEndTime;
                if (currentOfficer.key < currentPassenger->arrivalTime){
                    inspectionEndTime = currentPassenger->arrivalTime + currentOfficer.inspectionTime;
                } else {
                    inspectionEndTime = currentOfficer.key + currentOfficer.inspectionTime;
                }
                currentPassenger->customsClearedTime = inspectionEndTime;
                currentOfficer.key = inspectionEndTime;
                officerHeaps[officerCatSwitch]->insertKey(currentOfficer);
                currentPassenger->visistedOfficerId = currentOfficer.id;

                passengersListSorted.addNext(*currentPassenger);
                passengersListSorted.sortList();
                linePosition = -1;
            }
            linePosition++;
            readBuffer.clear();
        } else {
            readBuffer.read(currentSymbol);
        }
    }

    if (! onePassengerRead && currentSymbol == 'X') {
            fout << "nothing";
            fin.close();
            return 0;
    }

    for (passengersListSorted.start(); !passengersListSorted.end(); passengersListSorted.next()) {
        fout    << passengersListSorted.current->value.arrivalTime 
                << " " 
                << passengersListSorted.current->value.customsClearedTime 
                // << " " 
                // << endl
                // << passengersListSorted.current->value.visistedOfficerId 
                << endl;
    }

    fin.close();

    return 0;
}